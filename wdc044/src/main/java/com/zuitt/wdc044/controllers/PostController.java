package com.zuitt.wdc044.controllers;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostServiceImpl;
import com.zuitt.wdc044.services.PostService;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class PostController {
    

    @Autowired
    PostServiceImpl postServiceImpl;

    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<?> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postServiceImpl.createPost(stringToken, post);

        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }
    //Controller for getting post
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts(){
        return new ResponseEntity<>(postServiceImpl.getPosts(), HttpStatus.OK);
    }
    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Post>> getUserPosts(@RequestHeader(value = "Authorization") String stringToken) {
        Iterable<Post> myPosts = postServiceImpl.getMyPosts(stringToken);
        return new ResponseEntity<>(myPosts, HttpStatus.OK);
    }



    @RequestMapping(value="/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        return postServiceImpl.updatePost(postid, stringToken, post);


    }

    @RequestMapping(value="/post/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable Long postid,@RequestHeader(value = "Authorization") String stringToken){

        return postServiceImpl.deletePost(postid, stringToken);
    }


}
